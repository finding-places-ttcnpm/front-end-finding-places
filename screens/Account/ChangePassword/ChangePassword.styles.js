import { StyleSheet } from "react-native";

export default StyleSheet.create({
  container: {
    flex: 1,
    padding: 10
  },
  header: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  headerLogo: {
    fontSize: 30,
    fontWeight: "bold"
  },
  body: {
    flex: 5,
    justifyContent: "center",
    alignItems: "center"
  },
  buttonForget: {
    alignSelf: "flex-end",
    margin: 10,
    marginRight: 40
  },
  buttonForgetText: {
    fontSize: 10,
    color: "#0000cd",
    fontFamily: "MontserratSemiBold"
  },
  footer: {
    flex: 3,
    justifyContent: "center",
    alignItems: "center"
  }
});
