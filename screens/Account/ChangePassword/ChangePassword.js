import React from "react";
import {
  Text,
  View,
  KeyboardAvoidingView,
  TouchableOpacity
} from "react-native";
import PropTypes from "prop-types";

import Button from "../../../components/common/Button/Button";
import Header from "../../../components/common/Header/Header";
import TextField from "../../../components/common/TextField/TextField";
import styles from "./ChangePassword.styles.js";

const ChangePassword = props => {
  return (
    <KeyboardAvoidingView style={styles.container} enabled behavior="padding">
      <View style={styles.header}>
        <Header
          onPress={() => props.navigation.navigate("Account")}
          cancel={true}
          title="Thay đổi mật khẩu"
        />
      </View>
      <View style={styles.body}>
        <TextField title="Mật khẩu cũ" />
        <TextField title="Mật khẩu mới" />
        <TextField title="Nhập lại mật khẩu" />
      </View>
      <View style={styles.footer}>
        <Button
          title="Lưu"
          onPress={() => props.navigation.navigate("Account")}
        />
      </View>
    </KeyboardAvoidingView>
  );
};

ChangePassword.propTypes = {};

export default ChangePassword;
