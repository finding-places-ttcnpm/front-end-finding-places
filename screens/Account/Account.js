import React from "react";
import {
  Text,
  View,
  KeyboardAvoidingView,
  TouchableOpacity
} from "react-native";
import PropTypes from "prop-types";

import Button from "../../components/common/Button/Button";
import Header from "../../components/common/Header/Header";
import TextField from "../../components/common/TextField/TextField";
import styles from "./Account.styles.js";

const Account = props => {
  return (
    <KeyboardAvoidingView style={styles.container} enabled behavior="padding">
      <View style={styles.header}>
        <Header cancel={false} title="Thông tin cá nhân" />
      </View>
      <View style={styles.body}>
        <TextField title="Tên hiển thị" />
        <TextField title="Email" />
        <TextField title="Số điện thoại" />
        <TextField title="Số địa điểm đã thêm" />
        <TouchableOpacity
          onPress={() => props.navigation.navigate("ChangePassword")}
          style={styles.buttonForget}
        >
          <Text style={styles.buttonForgetText}>Thay đổi mật khẩu</Text>
        </TouchableOpacity>
      </View>
      <View style={styles.footer}>
        <Button title="Lưu" />
      </View>
    </KeyboardAvoidingView>
  );
};

Account.propTypes = {};

export default Account;
