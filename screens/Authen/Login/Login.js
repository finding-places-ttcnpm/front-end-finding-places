import React from "react";
import {
  Text,
  View,
  KeyboardAvoidingView,
  TouchableOpacity
} from "react-native";
import PropTypes from "prop-types";

import Button from "../../../components/common/Button/Button";
import Header from "../../../components/common/Header/Header";
import TextField from "../../../components/common/TextField/TextField";
import styles from "./Login.styles.js";

const Login = props => {
  return (
    <KeyboardAvoidingView style={styles.container} enabled behavior="padding">
      <View style={styles.header}>
        <Header
          cancel={true}
          title="Đăng nhập"
          onPress={() => props.navigation.navigate("Start")}
        />
      </View>
      <View style={styles.body}>
        <TextField title="Email" />
        <TextField title="Password" />
        <TouchableOpacity
          onPress={() => props.navigation.navigate("Forget")}
          style={styles.buttonForget}
        >
          <Text style={styles.buttonForgetText}>Quên mật khẩu</Text>
        </TouchableOpacity>
      </View>
      <View style={styles.footer}>
        <Button
          title="Đăng nhập"
          onPress={() => props.navigation.navigate("Home")}
        />
      </View>
    </KeyboardAvoidingView>
  );
};

Login.propTypes = {};

export default Login;
