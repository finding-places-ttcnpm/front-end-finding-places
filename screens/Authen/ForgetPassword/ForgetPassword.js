import React from "react";
import {
  Text,
  View,
  KeyboardAvoidingView,
  TouchableOpacity
} from "react-native";
import PropTypes from "prop-types";

import Button from "../../../components/common/Button/Button";
import Header from "../../../components/common/Header/Header";
import TextField from "../../../components/common/TextField/TextField";
import styles from "./ForgetPassword.styles.js";

const ForgetPassword = props => {
  return (
    <KeyboardAvoidingView style={styles.container} enabled behavior="padding">
      <View style={styles.header}>
        <Header
          cancel={true}
          title="Đăng Kí"
          onPress={() => props.navigation.navigate("Login")}
        />
      </View>
      <View style={styles.body}>
        <TextField title="Email" />
      </View>
      <View style={styles.footer}>
        <Button title="Gửi" />
      </View>
    </KeyboardAvoidingView>
  );
};

ForgetPassword.propTypes = {};

export default ForgetPassword;
