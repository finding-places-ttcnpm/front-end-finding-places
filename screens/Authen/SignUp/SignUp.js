import React from "react";
import {
  Text,
  View,
  KeyboardAvoidingView,
  TouchableOpacity
} from "react-native";
import PropTypes from "prop-types";

import Button from "../../../components/common/Button/Button";
import Header from "../../../components/common/Header/Header";
import TextField from "../../../components/common/TextField/TextField";
import styles from "./SignUp.styles.js";

const SignUp = props => {
  return (
    <KeyboardAvoidingView style={styles.container} enabled behavior="padding">
      <View style={styles.header}>
        <Header
          cancel={true}
          title="Đăng Ký"
          onPress={() => props.navigation.navigate("Start")}
        />
      </View>
      <View style={styles.body}>
        <TextField title="Email" />
        <TextField title="Số điện thoại" />
        <TextField title="Mật khẩu" />
        <TextField title="Nhập lại mật khẩu" />
      </View>
      <View style={styles.footer}>
        <Button title="Đăng kí" />
      </View>
    </KeyboardAvoidingView>
  );
};

SignUp.propTypes = {};

export default SignUp;
