import React, { Component } from "react";
import { Text, View } from "react-native";
import PropTypes from "prop-types";

import styles from "./StartScreen.styles";

import Button from "../../../components/common/Button/Button";

class StartScreen extends Component {
  handleLogin = () => {
    this.props.navigation.navigate("Login");
  };
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <Text style={styles.headerLogo}>Logo</Text>
        </View>
        <View style={styles.body}>
          <Button
            onPress={() => this.props.navigation.navigate("Login")}
            title="Đăng nhập"
            style={{ marginBottom: 20 }}
          />
          <Button
            onPress={() => this.props.navigation.navigate("SignUp")}
            title="Đăng kí"
            style={{ backgroundColor: "#E90F0E", shadowColor: "#CC0002" }}
          />
        </View>
      </View>
    );
  }
}

StartScreen.propTypes = {};

export default StartScreen;
