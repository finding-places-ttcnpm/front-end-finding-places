import { StyleSheet } from "react-native";

export default StyleSheet.create({
  container: {
    flex: 1
  },
  header: {
    flex: 4,
    justifyContent: "center",
    alignItems: "center"
  },
  headerLogo: {
    fontSize: 30,
    fontWeight: "bold"
  },
  body: {
    flex: 5,
    justifyContent: "center",
    alignItems: "center"
  }
});
