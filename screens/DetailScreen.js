import React, { Component } from 'react';

import { 
    Image,
    TouchableOpacity,
    StyleSheet,
    Button,
    ScrollView, 
    Text, View,
    Dimensions } from 'react-native';

import HeartButton from './components/common/HeartButton/heartbutton';
import StarRating from './components/common/StarRating/star-rating';


export default class DetailScreen extends Component {
    render() { 
        let screenWidth = Dimensions.get('window').width;      
        let screenHeight = Dimensions.get('window').height;    

        //for rating-star personal
        const ratingObj = {
            ratings: 1.5,
            views: -1
        }
        const ratingObjMain = {
            ratings: 2.5,
            views: 34000
        }
        const reviewComment = {
            content: "Lâu ghê mới quay lại uống phúc long, mình với bạn đi tầm giờ trưa nên quán khá đông khách luôn, lần này uống lại dòng trà đào quen thuộc nhưng rất ưng ý, món trà đào sữa cũng ok, không gian rộng nhưng lúc nào cũng full khách",
            name: "Nguyen Van A",
            time: "01/01/2020"
        }

        const storeDetail = {
            name: "Phúc Long Coffee & Tea Lê Văn Sỹ",
            address: "350 đường Lê Văn Sỹ, Phường 14, quận 3, Hồ Chí Minh",
            openinghour: "7:00 - 21:00",
            price: "30 - 80k",
            sumary: "Quán kinh doanh các loại cà phê, matcha,.. có không gian thoải mái, yên tĩnh, phù hợp cho học tập và làm việc"
        }

        return(
            <View style={styles.container}>
              <ScrollView>
                <View style={{height: 0.4*screenHeight}} >
                <ScrollView
                    horizontal={true}  
                    pagingEnabled={true}
                    showsHorizontalScrollIndicator={true}    
                    scrollIndicatorInsets={{top: 10, left: 10, bottom: 10, right: 10}} //ios                    
                    scrollEventThrottle={10}
                >
                    <View style={{
                        backgroundColor:'#5f9ea0',
                        width: screenWidth,                                 
                        justifyContent: 'center',
                        alignItems: 'center'
                    }}>
                        <Text style={styles.contentSwipeScreen}>Screen 1</Text> 
                        <TouchableOpacity style={{position:'absolute', right:10, top:10}}>
                            <Image 
                                style={{ width: 25, height: 25, resizeMode: "contain" }}
                                source={require('../components/common/close-browser.png')}
                            />
                        </TouchableOpacity>
                    </View>
                    
                    <View style={{
                        backgroundColor:'tomato',
                        flex: 1,
                        width: screenWidth,                
                        justifyContent: 'center',
                        alignItems: 'center'
                    }}>
                        <Text style={styles.contentSwipeScreen}>Screen 2</Text> 
                        <TouchableOpacity style={{position:'absolute', right:10, top:10}}>
                            <Image 
                                style={{ width: 25, height: 25, resizeMode: "contain" }}
                                source={require('../components/common/close-browser.png')}
                            />
                        </TouchableOpacity>
                    </View>
                    <View style={{
                        backgroundColor:'#663399',
                        flex: 1,
                        width: screenWidth,                 
                        justifyContent: 'center',
                        alignItems: 'center'
                    }}>
                        <Text style={styles.contentSwipeScreen}>Screen 3</Text> 
                        <TouchableOpacity style={{position:'absolute', right:10, top:10}}>
                            <Image 
                                style={{ width: 25, height: 25, resizeMode: "contain" }}
                                source={require('../components/common/close-browser.png')}
                            />
                        </TouchableOpacity>
                    </View>
                </ScrollView>
                </View>
                {/* scroll view cua thanh phan ben duoi */}
                <View style={{margin: 25}}>
                    {/* view ten cua hang + dia chi + favorite */}
                    <View style={{flexDirection: 'row', alignItems: "center"}}>
                        <View style={{marginRight:40}}>    
                            <Text style={styles.storeName}>{storeDetail.name}</Text>
                            <Text style={styles.storeAddress}>{storeDetail.address}</Text>
                        </View>
                        <HeartButton />
                    </View>
                    {/* view danh gia + gio mo cua + binh luan */}
                    <View style={{flexDirection: 'row', alignItems: "center"}}>
                        <Text>Đánh giá: </Text>
                        <StarRating ratingObj={ratingObjMain}/>
                    </View>
                    <View style={{flexDirection: 'row', alignItems: "center", marginBottom:10}}>
                        <View style={{fontFamily:"Montserrat-Medium", fontSize:12}}>
                            <Text>Giờ mở cửa: {storeDetail.openinghour}</Text>
                            <Text>Giá: {storeDetail.price}</Text>
                        </View>
                        <View style={styles.buttonTitleComment}>
                            <Button 
                                title="Bình Luận"
                            />
                        </View>                          
                    </View> 
                                
                    <Text>{storeDetail.sumary}</Text>
                    
                    <View style={styles.lineDetailReview}/>

                    {/* view review cua khach hang */}
                    {/* review so 1 */}
                    <View>
                        <Text style={styles.commentName}>{reviewComment.name}</Text>
                        <Text style={styles.commentDate}>{reviewComment.time}</Text>
                        <StarRating ratingObj={ratingObj}/>
                        <Text style={styles.commentContent}>{reviewComment.content}</Text>
                        <View style={styles.lineReview} />
                    </View>
                    {/* review so 2 */}
                    <View>
                        <Text style={styles.commentName}>{reviewComment.name}</Text>
                        <Text style={styles.commentDate}>{reviewComment.time}</Text>
                        <StarRating ratingObj={ratingObj}/>
                        <Text style={styles.commentContent}>{reviewComment.content}</Text>
                        <View style={styles.lineReview} />
                    </View>
                    {/* review so 3 */}
                    <View>
                        <Text style={styles.commentName}>{reviewComment.name}</Text>
                        <Text style={styles.commentDate}>{reviewComment.time}</Text>
                        <StarRating ratingObj={ratingObj}/>
                        <Text style={styles.commentContent}>{reviewComment.content}</Text>
                        <View style={styles.lineReview} />
                    </View>

                </View>    
                </ScrollView>
            </View>
            
            );        
        }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'stretch'
    },

    storeName:{
        fontFamily: "Montserrat-SemiBold", 
        fontSize:25
    },
    storeAddress:{
        fontFamily:"Montserrat-Light", 
        fontSize:10
    },
    contentSwipeScreen: {
        fontSize:20, 
        padding: 15, 
        color: 'white',                                            
        textAlign: 'center'
    },
    buttonTitleComment: {
        position: 'absolute', 
        right: 0
    },

    lineDetailReview: {
        marginTop: 10,
        borderBottomColor: '#191970',
        borderBottomWidth: 0.5
    },
    commentName:{
        fontFamily:"Montserrat-Regular",
        fontSize: 14
    },
    commentDate:{
        fontFamily:"Montserrat-Regular",
        fontSize: 10
    },
    commentContent: {
        fontFamily:"Montserrat-Regular",
        fontSize: 12
    },
    lineReview: {
        borderBottomColor: '#EDEDED',
        borderBottomWidth: 0.5,
        margin: 10
    }
})