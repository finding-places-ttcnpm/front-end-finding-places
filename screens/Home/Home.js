import React from "react";
import { View, Text, TouchableOpacity } from "react-native";
import PropTypes from "prop-types";
import { ScrollView } from "react-native-gesture-handler";

import styles from "./Home.styles";

import bora from "../../assets/images/bora.jpg";

import Menu from "./Menu/Menu";
import Place from "../../components/common/Place/Place";
import TextField from "../../components/common/TextField/TextField";
const Home = props => {
  return (
    <ScrollView style={styles.container}>
      <View style={styles.header}>
        <TouchableOpacity style={styles.buttonHome}>
          <Text style={styles.buttonText}>Home</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={{ ...styles.buttonMap, backgroundColor: "#fff" }}
        >
          <Text style={{ ...styles.buttonText, color: "#3B7CEC" }}>Map</Text>
        </TouchableOpacity>
      </View>
      <View style={styles.menu}>
        <Menu />
      </View>
      <View style={styles.news}>
        <Text style={styles.title}>Điểm địa vừa thêm</Text>
        <Place
          source={bora}
          title="Caribbean"
          note="999 Điện Biên Phủ, Q9, HCM"
          content="It’s a rather precarious task to decide upon the “most beautiful place..."
        />
        <Place
          source={bora}
          title="Caribbean"
          note="999 Điện Biên Phủ, Q9, HCM"
          content="It’s a rather precarious task to decide upon the “most beautiful place..."
        />
        <Place
          source={bora}
          title="Caribbean"
          note="999 Điện Biên Phủ, Q9, HCM"
          content="It’s a rather precarious task to decide upon the “most beautiful place..."
        />
      </View>
    </ScrollView>
  );
};

Home.propTypes = {};

export default Home;
