import { StyleSheet } from "react-native";

export default StyleSheet.create({
  container: {
    flex: 1
  },
  header: {
    height: 100,
    width: "100%",
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "row"
  },
  buttonHome: {
    borderColor: "#3B7CED",
    borderTopLeftRadius: 40,
    borderBottomLeftRadius: 40,
    borderWidth: 1,
    width: 80,
    height: 35,
    backgroundColor: "#3B7CED",
    justifyContent: "center",
    alignItems: "center"
  },
  buttonMap: {
    borderColor: "#3B7CED",
    borderTopRightRadius: 40,
    borderBottomRightRadius: 40,
    borderWidth: 1,
    width: 80,
    height: 35,
    backgroundColor: "#3B7CED",
    justifyContent: "center",
    alignItems: "center"
  },
  buttonText: {
    color: "#fff",
    fontFamily: "MontserratSemiBold",
    fontSize: 14
  },
  menu: {
    height: 325,
    paddingLeft: 10,
    paddingRight: 10,
    justifyContent: "flex-start",
    alignItems: "center"
  },
  news: {
    padding: 10
  },
  title: {
    fontFamily: "MontserratBlack",
    fontSize: 20
  }
});
