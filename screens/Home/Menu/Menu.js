import React from "react";
import { View } from "react-native";
import PropTypes from "prop-types";

import styles from "./Menu.styles";

import Icon from "./Icon/Icon";

import grocery from "../../../assets/images/icons/grocery.png";
const Menu = props => {
  return (
    <View style={styles.container}>
      <Icon source={grocery} title="Quán ăn" />
      <Icon source={grocery} title="Quán nuớc" />
      <Icon source={grocery} title="Đi chợ" />
      <Icon source={grocery} title="Khách sạn" />
      <Icon source={grocery} title="Trạm xe bus" />
      <Icon source={grocery} title="Rạp phim" />
      <Icon source={grocery} title="Ngân hàng" />
      <Icon source={grocery} title="ATM" />
      <Icon source={grocery} title="Nhà vệ sinh" />
      <Icon source={grocery} title="Bệnh viện" />
      <Icon source={grocery} title="Cây xăng" />
      <Icon source={grocery} title="Khác" />
    </View>
  );
};

Menu.propTypes = {};

export default Menu;
