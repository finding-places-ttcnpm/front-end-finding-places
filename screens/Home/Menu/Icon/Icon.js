import React from "react";
import { TouchableOpacity, Image, Text, View } from "react-native";
import PropTypes from "prop-types";

import styles from "./Icon.styles";

const Icon = ({ source, title }) => {
  return (
    <View style={styles.container}>
      <TouchableOpacity style={styles.button}>
        <Image source={source} style={styles.image} />
      </TouchableOpacity>
      <Text style={styles.title}>{title}</Text>
    </View>
  );
};

Icon.propTypes = {};

export default Icon;
