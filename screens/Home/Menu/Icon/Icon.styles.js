import { StyleSheet } from "react-native";

export default StyleSheet.create({
  container: {
    margin: 5,
    alignItems: "center"
  },
  button: {
    height: 70,
    width: 70,
    borderRadius: 10,
    borderWidth: 1,
    borderColor: "#ddd",
    backgroundColor: "#fff",
    justifyContent: "center",
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 6
    },
    shadowOpacity: 0.39,
    shadowRadius: 8.3,

    elevation: 13,
    marginBottom: 5
  },
  image: {
    height: 30,
    width: 30,
    resizeMode: "contain"
  },
  title: {
    fontSize: 11,
    fontFamily: "MontserratSemiBold"
  }
});
