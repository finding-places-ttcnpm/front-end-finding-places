import { apiFetch } from "../../services/api.services";

export const fetchUserService = username => {
  return apiFetch().get(`/users/${username}`);
};
