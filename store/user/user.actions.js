import { SET_USER } from "./user.types";
import { fetchUserService } from "./user.services";
import { Alert } from "react-native";

export const setUser = user => ({
  type: SET_USER,
  user
});

export const fetchUser = username => {
  return async dispatch => {
    try {
      const response = await fetchUserService(username);
      dispatch(setUser(response.data));
    } catch (error) {
      Alert.alert("Error", "alert.message", [{ text: "OK" }], {
        cancelable: true
      });
    }
  };
};
