import { combineReducers } from "redux";
import userReducer from "./user/user.reducer";
// import alertReducer from "./alert/alert.reducer";
// import loadingReducer from "./loading/loading.reducer";
// import cameraReducer from "./camera/camera.reducer";

const rootReducer = combineReducers({
  user: userReducer
  //   alert: alertReducer,
  //   loading: loadingReducer
});

export default rootReducer;
