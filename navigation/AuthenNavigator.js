import React from "react";
import { TouchableOpacity, Image } from "react-native";
import { createStackNavigator } from "react-navigation";

import StartScreen from "../screens/Authen/StartScreen/StartScreen";
import Login from "../screens/Authen/Login/Login";
import SignUp from "../screens/Authen/SignUp/SignUp";
import ForgetPassword from "../screens/Authen/ForgetPassword/ForgetPassword";

import MainTabNavigator from "./MainTabNavigator";

import Account from "../screens/Account/Account";
import ChangePassword from "../screens/Account/ChangePassword/ChangePassword";
const AuthenNavigator = createStackNavigator(
  {
    Start: StartScreen,
    Login: Login,
    SignUp: SignUp,
    Forget: ForgetPassword,
    Home: MainTabNavigator,
    Account: Account,
    ChangePassword: ChangePassword
  },
  {
    defaultNavigationOptions: {
      header: null
    }
  }
);

export default AuthenNavigator;
