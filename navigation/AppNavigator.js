import React from "react";
import { createAppContainer, createSwitchNavigator } from "react-navigation";

import AuthenNavigator from "./AuthenNavigator";
import AppDrawerNavigator from "./AppDrawerNavigator";
import StartScreen from "../screens/Authen/StartScreen/StartScreen";
const AuthStack = createSwitchNavigator({
  AuthenNavigator: {
    screen: AuthenNavigator,
    navigationOptions: {
      header: null
    }
  }
});
export default createAppContainer(
  createSwitchNavigator(
    {
      AuthLoading: StartScreen,
      App: AppDrawerNavigator,
      Auth: AuthStack
    },
    {
      initialRouteName: "AuthLoading"
    }
  )
);
