import React from "react";
import { TouchableOpacity, Image, StyleSheet } from "react-native";
import { createStackNavigator } from "react-navigation";

import AuthenNavigator from "./AuthenNavigator";

const HomeNavigator = createStackNavigator({
  Authen: {
    screen: AuthenNavigator,
    navigationOptions: {
      header: null
    }
  }
});

export default HomeNavigator;
