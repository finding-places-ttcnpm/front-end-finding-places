// import React from "react";
import { createDrawerNavigator } from "react-navigation";

import HomeNavigator from "./HomeNavigator";

const AppDrawerNavigator = createDrawerNavigator(
  {
    "Find a Lactation Room": {
      screen: HomeNavigator
    }
  },
  {
    contentOptions: {
      labelStyle: {
        fontWeight: "normal",
        fontSize: 16,
        color: "#fff"
      },
      itemsContainerStyle: {
        marginLeft: 20,
        marginTop: 20
      },
      iconContainerStyle: {
        opacity: 1
      }
    }
  }
);

export default AppDrawerNavigator;
