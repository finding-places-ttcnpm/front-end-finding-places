import React from "react";
import { Platform, Image, View } from "react-native";
import {
  createStackNavigator,
  createBottomTabNavigator
} from "react-navigation";

import TabBarIcon from "../components/TabBarIcon";
import HomeScreen from "../screens/Home/Home";
import SearchScreen from "../screens/Search/Search";
import FavouriteScreen from "../screens/Favourite/Favourite";
import AccountScreen from "../screens/Account/Account";
import AddPlaceScreen from "../screens/AddPlace/AddPlace";

import iconHome from "../assets/images/icons/home.png";
import iconSearch from "../assets/images/icons/search.png";
import iconAdd from "../assets/images/icons/add.png";
import iconFavourite from "../assets/images/icons/favourite.png";
import iconAccount from "../assets/images/icons/account.png";

const HomeStack = createStackNavigator({
  Home: {
    screen: HomeScreen,
    navigationOptions: {
      header: null
    }
  }
});

HomeStack.navigationOptions = {
  tabBarLabel: "Home",
  tabBarIcon: ({ focused }) => (
    <Image
      source={iconHome}
      style={{ width: 20, height: 20, tintColor: focused ? "blue" : "#ddd" }}
    />
  )
};

const SearchStack = createStackNavigator({
  Search: SearchScreen
});

SearchStack.navigationOptions = {
  tabBarLabel: "Search",
  tabBarIcon: ({ focused }) => (
    <Image
      source={iconSearch}
      style={{ width: 20, height: 20, tintColor: focused ? "blue" : "#ddd" }}
    />
  )
};

const AddPlaceStack = createStackNavigator({
  AddPlace: {
    screen: AddPlaceScreen
  }
});

AddPlaceStack.navigationOptions = {
  tabBarIcon: ({ focused }) => (
    <Image
      source={iconAdd}
      style={{
        width: 50,
        height: 50,
        tintColor: focused ? "#0000cc" : "#3B7CEC"
      }}
    />
  )
};

const FavouriteStack = createStackNavigator({
  Favourite: FavouriteScreen
});

FavouriteStack.navigationOptions = {
  tabBarLabel: "Favourite",
  tabBarIcon: ({ focused }) => (
    <Image
      source={iconFavourite}
      style={{ width: 20, height: 20, tintColor: focused ? "blue" : "#ddd" }}
    />
  )
};

const AccountStack = createStackNavigator({
  Account: {
    screen: AccountScreen,
    navigationOptions: {
      header: null
    }
  }
});

AccountStack.navigationOptions = {
  tabBarLabel: "Account",
  tabBarIcon: ({ focused }) => (
    <Image
      source={iconAccount}
      style={{
        width: 20,
        height: 20,
        tintColor: focused ? "blue" : "#ddd"
      }}
    />
  )
};

export default createBottomTabNavigator(
  {
    HomeStack,
    SearchStack,
    AddPlaceStack,
    FavouriteStack,
    AccountStack
  },
  {
    tabBarOptions: { showLabel: false }
  }
);
