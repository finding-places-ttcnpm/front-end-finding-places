import axios from "axios";
// import { SERVER_API } from "../constants/configAPI";
const SERVER_API = "https://api.github.com";

export const apiFetch = () => {
  let instance = axios.create({
    baseURL: SERVER_API
  });

  instance.interceptors.response.use(
    res => {
      console.log(JSON.stringify(res));
      return res;
    },
    error => {
      //   if (error.response && error.response.status === 401) {
      //     // history.replace("/logout");
      //   }

      //   error.response && console.log(error.response);
      return Promise.reject(error);
    }
  );

  return instance;
};
