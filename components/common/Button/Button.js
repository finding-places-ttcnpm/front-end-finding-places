import React from "react";
import { TouchableOpacity, Text } from "react-native";
import PropTypes from "prop-types";

import styles from "./Button.styles";
const Button = props => {
  return (
    <TouchableOpacity
      style={{
        ...styles.container,
        ...props.style
      }}
      onPress={props.onPress}
    >
      <Text style={styles.text}>{props.title}</Text>
    </TouchableOpacity>
  );
};

Button.propTypes = {};

export default Button;
