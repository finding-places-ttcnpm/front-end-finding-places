import { StyleSheet } from "react-native";

export default StyleSheet.create({
  container: {
    width: 247,
    height: 40,
    backgroundColor: "#0000cc",
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 20,
    margin: 5,
    shadowColor: "#0000cc",
    shadowOffset: {
      width: 0,
      height: 6
    },
    shadowOpacity: 0.39,
    shadowRadius: 8.3
  },
  text: {
    color: "#fff",
    fontSize: 14,
    fontFamily: "MontserratBold"
  }
});
