import React, { Component } from "react";
import { View, TextInput, Text } from "react-native";
import PropTypes from "prop-types";

import styles from "./TextField.styles";

class TextField extends Component {
  render() {
    const { title, placeholder, value, onPress, inputProps } = this.props;

    return (
      <View
        style={styles.container}
        // onPress={this.handleClick(onPress)}
      >
        <Text style={styles.title}>{title}</Text>
        <TextInput
          placeholder={placeholder}
          style={styles.input}
          {...inputProps}
          value={value}
        />
      </View>
    );
  }
}

TextField.propTypes = {};

export default TextField;
