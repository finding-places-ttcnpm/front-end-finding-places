import React, { Component } from 'react';
import {TouchableOpacity,
	StyleSheet,
	View,
	Image,
	Text
} from 'react-native';


import soundImg from './heart-solid.png';
import muteImg from './heart-regular.png';

export default class HeartButton extends Component {
  constructor() {
    super();
    this.state = { showSoundImg: false };
  }
  static navigationOptions = {
    header: null,
  };
  renderImage(){
    var imgSource = this.state.showSoundImg? soundImg : muteImg;
    return (
      <Image
        style={{width: 30, height: 30}}
        source={ imgSource }
      />
    );
  }
  render(){
    return (
      <View >
        <TouchableOpacity onPress={() => {
          this.setState({ showSoundImg: !this.state.showSoundImg})
              }} 
          activeOpacity={0.5}>
            {this.renderImage()}
          <View />
          </TouchableOpacity>
      </View>
    );
  }
}