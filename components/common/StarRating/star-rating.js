// @flow
import React, { Component } from 'react';
import {
	StyleSheet,
	View,
	Image,
	Text
} from 'react-native';

type Props = {
	ratingObj : {
		ratings: number;
		views: number;
	}
};

export default class StarRating extends Component<Props> {
	render() {
		// Recieve the ratings object from the props
		let ratingObj = this.props.ratingObj;

		// This array will contain our star tags. We will include this
		// array between the view tag.
		let stars = [];
		// Loop 5 times
		for (var i = 1; i <= 5; i++) {
			// set the path to filled stars
			let path = require('./star-filled.png');
			// If ratings is lower, set the path to unfilled stars
			if ((i - ratingObj.ratings) == 0.5){
				path = require('./star-half.png');
			}
			else if (i > ratingObj.ratings) {
				path = require('./star-unfilled.png');
			}

			stars.push((<Image style={styles.image} source={path} />));
		}
		if (ratingObj.views == -1){
			return (
				<View style={ styles.container }>
					{ stars }
				</View>
			);
		}
		else{
			return (
				<View style={ styles.container }>
					{ stars }
					<View style={{flexDirection: 'row', alignItems: "center"}}>
						<Text style={styles.ratingNumber}>{ratingObj.ratings}</Text>
						<Text style={styles.text}>({ratingObj.views} lượt)</Text>
					</View>
				</View>
			);
		}


	}
}

const styles = StyleSheet.create({
	container: {
		flexDirection: 'row',
		alignItems: 'center'
	},
	image: {
		width: 25,
		height: 25
	},
	ratingNumber: {
		fontFamily: "Montserrat-SemiBold",
		fontSize: 14
	},
	text: {
		fontFamily: "Montserrat-Light",
		fontSize: 10
	}
});