import React from "react";
import { View, Text, TouchableOpacity, Image } from "react-native";
import PropTypes from "prop-types";

import cancelIcon from "../../../assets/images/icons/cancel.png";

import styles from "./Header.styles";

const Header = props => {
  return (
    <View style={styles.container}>
      <Text style={styles.title}>{props.title}</Text>
      {props.cancel && (
        <TouchableOpacity onPress={props.onPress}>
          <Image source={cancelIcon} style={styles.button} />
        </TouchableOpacity>
      )}
    </View>
  );
};

Header.propTypes = {};

export default Header;
