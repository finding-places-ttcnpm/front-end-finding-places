import { StyleSheet } from "react-native";

export default StyleSheet.create({
  container: {
    width: "100%",
    height: 100,
    justifyContent: "space-between",
    paddingLeft: 20,
    paddingRight: 20,
    alignItems: "center",
    borderBottomColor: "#CAC9C8",
    borderBottomWidth: 0.2,
    flexDirection: "row"
  },
  title: {
    fontSize: 24,
    fontFamily: "MontserratBlack"
  },
  button: {
    width: 28,
    height: 28
  }
});
